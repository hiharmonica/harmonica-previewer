import React from "react";
import ReactDOM from "react-dom";

import Main from "./Main";

import "./index.less";

ReactDOM.render(<Main />, document.getElementById("app"));
