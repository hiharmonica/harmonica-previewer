FROM ubuntu:20.04 AS build

ENV DEBIAN_FRONTEND=noninteractive

# install build-tools
WORKDIR /cmake-builder
COPY CMake-3.18.4.tar.gz .

RUN rm -rf /bin/sh && \
    ln -s /bin/bash /bin/sh

RUN sed -i 's/archive.ubuntu.com/mirrors.aliyun.com/g' /etc/apt/sources.list && \
    sed -i 's/security.ubuntu.com/mirrors.aliyun.com/g' /etc/apt/sources.list && \
    apt-get clean -y && \
    apt-get -y update && \
    apt-get -y install build-essential && \
    apt-get -y install git curl openssl libssl-dev && \
    tar -zxvf CMake-3.18.4.tar.gz
RUN cd CMake-3.18.4 && \
    ./bootstrap && \
    make && make install

# BUILD LIB
WORKDIR /lib-builder

RUN apt-get remove python* -y && \
    apt-get install python3-pip -y && \
    apt-get -y install python3-pip && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1 && \
    curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/bin/repo && \
    chmod a+x /usr/bin/repo && \
    pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple requests

RUN git config --global user.email "william1994dx@163.com" && \
    git config --global user.name "zh-jn" && \
    git config --global color.ui false && \
    git config --global credential.helper store && \
    repo init -u https://gitee.com/openharmony/manifest.git -b master --repo-branch=stable --no-repo-verify && \
    repo sync -c

RUN echo "a"

COPY Makefiles/Harmony/foundation/ace/frameworks/lite/targets/simulator/win/acelite_config.h foundation/ace/frameworks/lite/targets/simulator/
COPY Makefiles/Harmony/foundation/graphic/lite/interfaces/kits/config/graphic_config.h foundation/graphic/lite/interfaces/kits/config/
COPY Makefiles/Harmony/third_party/bounds_checking_function/CMakeLists.txt third_party/bounds_checking_function/
COPY Makefiles/Harmony/third_party/libpng/CMakeLists.txt third_party/libpng/
COPY Makefiles/Harmony/third_party/zlib/CMakeLists.txt third_party/zlib/
COPY Makefiles/Harmony/third_party/jerryscript/CMakeLists.txt third_party/jerryscript/
COPY Makefiles/Harmony/CMakeLists.txt .

RUN rm -rf cmake-build && \
    mkdir -p cmake-build && \
    cd cmake-build && \
    cmake .. && \
    make

# BUILD SERVER
WORKDIR /server-builder
COPY Server .
# copy lib
RUN cd Harmony/lib/ && \
    rm -rf ./* && \ 
    cp /lib-builder/cmake-build/base/global/frameworks/resmgr_lite/libglobal_resmgr.a . && \
    cp /lib-builder/cmake-build/foundation/ace/frameworks/lite/libjsfwk.a . && \
    cp /lib-builder/cmake-build/foundation/ace/frameworks/lite/jerryscript/lib/* . && \
    cp /lib-builder/cmake-build/foundation/graphic/lite/libui.a . && \
    cp /lib-builder/cmake-build/foundation/graphic/lite/freetype.out/libfreetype.a . && \
    cp /lib-builder/cmake-build/foundation/graphic/lite/libjpeg.out/liblibjpeg.a . && \
    cp /lib-builder/cmake-build/foundation/graphic/lite/libpng.out/liblibpng.a . && \
    cp /lib-builder/cmake-build/foundation/graphic/lite/zlib.out/libzlib.a . && \
    cp /lib-builder/cmake-build/utils/native/lite/js/builtin/libutils_jsapi.a . && \
    cp /lib-builder/cmake-build/third_party/bounds_checking_function/liblibbcfunc.a .

WORKDIR /server-builder
RUN mkdir -p build && \
    cd build && \
    rm -rf ./* && \
    cmake .. && \
    make

# BUILD WEB
WORKDIR /web-builder
COPY previewer .

# install build tools
RUN apt clean -y && \
    apt -y update && \
    apt install -y npm && \
    npm config set registry https://registry.npm.taobao.org && \
    npm -g install npm

RUN npm -g install yarn && \
    yarn build

FROM ubuntu:20.04 AS release

WORKDIR /harmonica
COPY --from=build /server-builder/build/Previewer .
COPY --from=build /web-builder/dist www/
RUN mkdir -p config && mkdir -p entry
RUN echo '{"devices":[{"deviceType":"smartVision","vendor":"A Company","JSHeapinK":88,"Screen":{"width":454,"height":454,"shape":"round"},"commands":[{"name":"mouseMove","args":"x,y"},{"name":"GeoLocation","args":"longitude, latitude"},{"name":"Temperature","args":"value"}]}]}' > config/config.json

ENTRYPOINT [ "./Previewer" ]