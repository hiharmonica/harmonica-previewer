# 开发环境
```
操作系统：win10
编译器： gcc >= 7.3(可以使用 Qt 5.14.2自带的 gcc 7.3版本，也可以用tdm gcc)
cmake
```

## 三方库
cxxopts: MIT license，已经包含在源码仓库，来源`https://github.com/jarro2783/cxxopts.git`
asio: 已经包含在源码仓库
websocketpp:包含在源码仓库
cJSON: 包含在源码仓库

## 编译Server
```
cd Server
mkdir build
cd build
cmake .. -G "Unix Makefiles"
make
```

## 验证
1. 在命令行下输入`./Previewer -h`,显示如下帮助信息
```
harmonyos simulator
Usage:
  Previewer [OPTION...]

  -s, --simulatorPath arg  simulator local path
  -j, --jsBundle arg       jsBundle path
  -c, --devConfigPath arg  devConfigPath path
  -v, --deviceType arg     device type
  -p, --webPort arg        http port (default: 4096)
  -b, --projectName arg    project name (default: harmonyos project)
      --version            version
  -h, --help               produce help message
```

2. 启动`./Previewer`进程，默认监听4096端口，可以通过websocket测试工具连接 ws://localhost:4096 ，然后发送下面信息进行测试
```
{
	"name":"plugin",
	"command":"restart",
	"args": {
		"jsBundle":"blabla",
		"deviceType":"blabla"
	}
}
```