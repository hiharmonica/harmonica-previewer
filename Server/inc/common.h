#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>

#include "json.h"
#include "server.h"
#include "cli.h"

#ifdef WIN32
#include <direct.h>
#include <io.h>
#elif __linux__
#include <stdarg.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#ifdef WIN32
#define MKDIR(a) _mkdir((a))
#elif __linux__
#define MKDIR(a) mkdir((a),0755)
#endif


extern unsigned short port;
extern server Simulator_Server;
extern websocketpp::connection_hdl web_hdl;
extern bool m_send;
extern bool isChanged;
extern ProtocolMessage protocalMsg;
extern CliParam cliParam;
extern bool isAppRunning;

/**
 * ide发给管理服务的消息封装类
 * command: exit, restart, theme
 * {
 *      "name":"plugin",
 *      "command":"restart",
 *      "args":
 *      {
 *          "jsBundle":"blabla",
 *          "deviceType":"blabla"
 *      }
 * }
 */ 

struct CMessage {
    std::string name;
    std::string command;
    std::map<std::string, std::string> args;
    static CMessage parseJson(const std::string& json);
    std::string toString();
};

struct PMessage {
    std::string name;
    std::string command;
    std::map<std::string, int> args;
    static PMessage parseJson(const std::string &json);
    std::string toString();
};



bool ReadFileContent(const char* pFilePath, std::ios_base::openmode mode,std::string& strContent);

std::string TrimUTF8BOM(const std::string& msg);

void replace_all(std::string& str, const std::string old_value, const std::string new_value);
std::string ApplicationPath();


#endif // COMMON_H
