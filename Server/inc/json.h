#ifndef HARMONICA_SERVER_JSON_H
#define HARMONICA_SERVER_JSON_H

#include<string>
#include <vector>

/**
example layout:
{
    "devices": [
        {
            "deviceType": "smartVision",
            "vendor": "A Company",
            "JSHeapinK": 88,
            "Screen": {
                "width": 960,
                "height": 480,
                "shape": "round"
            },
            "commands": [
                {
                    "name": "mouseMove",
                    "args": "x,y"
                },
                {
                    "name": "GeoLocation",
                    "args": "longitude, latitude"
                },
                {
                    "name": "Temperature",
                    "args": "value"
                }
            ]
        }
    ]
}
*/


struct ProtocolScreen {
    uint16_t width;
    uint16_t height;
    std::string shape;
};

struct ProtocolCommand {
    std::string name;
    std::string args;
};

struct ProtocolDevice {
    std::string deviceType;
    std::string vendor;
    int jsHeapinK;
    ProtocolScreen screen;
    std::vector<ProtocolCommand> commands;
};

struct ProtocolMessage {
    std::vector<ProtocolDevice> devices;
    static ProtocolMessage parseMessage(std::string msg);
    std::string toString();
};


#endif // HARMONICA_SERVER_JSON_H
