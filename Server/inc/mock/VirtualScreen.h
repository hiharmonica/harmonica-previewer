#include "color.h"
#include "screen_device.h"
#include "input_device.h"
#include "MouseInput.h"
#include <string>

class VirtualScreen : public OHOS::ScreenDevice {
public:
    static VirtualScreen &GetInstance() {
        static VirtualScreen instance;
        return instance;
    }
    VirtualScreen(const VirtualScreen&) = delete;
    VirtualScreen& operator=(const VirtualScreen &) = delete;
    void Flush(int16_t x1, int16_t y1, int16_t x2, int16_t y2, const OHOS::ScreenBufferType* colorBuffer);
    void Init(uint16_t width, uint16_t height);
    uint16_t GetWidth() const;
    uint16_t GetHeight() const;
    uint8_t *screenBuffer = nullptr;
private:
    VirtualScreen();
    ~VirtualScreen();
};
