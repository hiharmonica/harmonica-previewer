#include "pointer_input_device.h"

class MouseInput : public OHOS::PointerInputDevice {
public:
    enum MouseStatus {INDEV_STATE_RELEASE = 0, INDEV_STATE_PRESS, INDEV_STATE_MOVE};
    static MouseInput &GetInstance() {
        static MouseInput instance;
        return instance;
    }
    bool Read(OHOS::DeviceData &data);
    double GetMouseXPosition();
    double GetMouseYPosition();
    void SetMouseStatus(MouseStatus status);
    void SetMousePosition(double x, double y);
    bool MouseRead(OHOS::DeviceData *data);
    MouseStatus GetMouseStatus();

private:
    MouseInput();
    ~MouseInput();
    MouseStatus mouseStatus;
    double mouseXPosition;
    double mouseYPosition;
};
