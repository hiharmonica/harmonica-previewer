#include <list>
#include <mutex>
#include <utility>

#include "js_async_work.h"

class AsyncWorkManager {
public:
    static AsyncWorkManager &GetInstance() {
        static AsyncWorkManager instance;
        return instance;
    }
    void AppendAsyncWork(OHOS::ACELite::AsyncWorkHandler work, void* arg);
    void ExecAllAsyncWork();
    void ClearAllAsyncWork();

private:
    AsyncWorkManager();
    ~AsyncWorkManager();
    std::mutex mutex;
    std::list<std::pair<OHOS::ACELite::AsyncWorkHandler , void*>> workList;
};
