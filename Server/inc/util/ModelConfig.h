#include <cstdint>
#include <string>

class ModelConfig {
public:
    ModelConfig(std::string model,
                bool isRound,
                int width,
                int height,
                int density,
                uint32_t jsMemory);
    ~ModelConfig() {}

    const std::string& GetModel() const;
    bool IsRoundScreen();
    const std::string& GetBrand();
    const std::string& GetManufacture();
    const std::string& GetProductModel();
    int GetScreenDensity();
    unsigned int GetJsMemorySize();
};
