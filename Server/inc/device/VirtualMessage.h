#include <string>
using namespace std;
class MessageInfo {
public:
    string bundleName;
    string message;
    string abilityName;
    string deviceID;
};

class VirtualMessage {
public:
    virtual void SendVirtualMessage(MessageInfo info);
};
