#ifndef JSAPP_H
#define JSAPP_H

#include <thread>
#include <string>
#include "js_ability.h"

class JsApp {
public:
    static JsApp &GetInstance();
    static void Run();
    void SetJsAppPath(const std::string& value);
    void SetUuid(std::string id);
    void SetJSHeapSize(uint32_t size);
    bool Start();
    bool Stop();
    uint16_t GetWidth() const;
    uint16_t GetHeight() const;
    void SetWidth(uint16_t width);
    void SetHeight(uint16_t height);

private:
    JsApp();
    virtual ~JsApp();
    static JsApp *instance;
    std::string jsAppPath;
    uint16_t width;
    uint16_t height;
    uint32_t JSHeapSize;
    std::string uuid;
    OHOS::ACELite::JSAbility *jsAbility;
    std::thread JsAppthread;
    void StartJsApp();
};

#endif //JSAPP_H
