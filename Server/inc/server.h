#ifndef SERVER_H
#define SERVER_H

#include <string>
#include <vector>
#include <list>

#include <iostream>
#include <fstream>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

typedef websocketpp::server<websocketpp::config::asio> server;
typedef server::message_ptr message_ptr;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

void on_run(server* s, unsigned int port);

void on_stop(server* s);

bool validate(server* s, websocketpp::connection_hdl);

void on_http(server* s, websocketpp::connection_hdl hdl);

void on_fail(server* s, websocketpp::connection_hdl hdl);

void on_open(server* s, websocketpp::connection_hdl hdl);

void on_close(websocketpp::connection_hdl hdl);

void on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg);

void sendMsg(server* s, websocketpp::connection_hdl hdl, std::string msg, websocketpp::frame::opcode::value codeType);

void sendImg(server* s, websocketpp::connection_hdl hdl, void const * payload, size_t len, websocketpp::frame::opcode::value op);

#endif //SERVER_H
