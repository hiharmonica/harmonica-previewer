#ifndef CLI_H
#define CLI_H


#include <iostream>
#include <string>
/**
 * 命令行参数
 */
struct CliParam
{
    std::string simulatorPath;
    std::string jsBundle;
    std::string devConfigPath;
    std::string deviceType;
    std::string projectName;
    unsigned short webPort = 0;
};


CliParam GetOptLong(int argc, char *argv[]);

#endif //CLI_H
