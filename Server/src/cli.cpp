#include "cli.h"

#include "cxxopts.hpp"

#include <fstream>


bool fileExist (const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}

CliParam GetOptLong(int argc, char *argv[]) {
    CliParam cliParam;
    cxxopts::Options options("Previewer", "harmonyos simulator");
    options.add_options() 
    ("s,simulatorPath","simulator local path",cxxopts::value<std::string>())
    ("j,jsBundle","jsBundle path",cxxopts::value<std::string>())
    ("c,devConfigPath","devConfigPath path",cxxopts::value<std::string>())
    ("v,deviceType","device type",cxxopts::value<std::string>())
    ("p,webPort","http port and websocket port",cxxopts::value<unsigned short>())
    ("b,projectName","project name",cxxopts::value<std::string>()->default_value("harmonyos project"))
    ("version","version")
    ("h,help", "produce help message");
    cxxopts::ParseResult vm;
    try{
        vm = options.parse(argc, argv);
    }catch(...){
        std::cerr << "argument error" << std::endl << options.help() << std::endl;
        exit(1);        
    }    
    if (vm.count("help") != 0) {
        std::cout << options.help() << std::endl;
        exit(0);
    }
    if (vm.count("simulatorPath") != 0) {
        cliParam.simulatorPath = vm["simulatorPath"].as<std::string>();
        if(cliParam.simulatorPath.empty()){
            std::cerr << "simulatorPath:" << cliParam.simulatorPath << " not exist" << std::endl;
        }
    }
    if (vm.count("jsBundle") != 0) {
        cliParam.jsBundle = vm["jsBundle"].as<std::string>();
        if(cliParam.jsBundle.empty()){
            std::cerr << "jsBundle:" << cliParam.jsBundle << " not exist" << std::endl;
        }        
    }
    if (vm.count("devConfigPath") != 0) {
        cliParam.devConfigPath = vm["devConfigPath"].as<std::string>();
        if(cliParam.devConfigPath.empty()){
            std::cerr << "devConfigPath:" << cliParam.devConfigPath << " not exist" << std::endl;
        }          
    }
    if (vm.count("deviceType") != 0) {
        cliParam.deviceType = vm["deviceType"].as<std::string>();
    }
    if (vm.count("webPort") != 0) {
        cliParam.webPort = vm["webPort"].as<unsigned short>();
    }
    if (vm.count("projectName") != 0) {
        cliParam.projectName = vm["projectName"].as<std::string>();
    }

    return std::move(cliParam);
}

