#include "MouseInput.h"
#include <iostream>

MouseInput::MouseInput() {}

MouseInput::~MouseInput() {}

bool MouseInput::Read(OHOS::DeviceData &data) {
    data.point.x = MouseInput::GetInstance().GetMouseXPosition();
    data.point.y = MouseInput::GetInstance().GetMouseYPosition();
    data.state = MouseInput::GetInstance().GetMouseStatus();
    return false;
}

double MouseInput::GetMouseXPosition() {
    return mouseXPosition;
}

double MouseInput::GetMouseYPosition() {
    return mouseYPosition;
}

void MouseInput::SetMouseStatus(MouseStatus status) {
    mouseStatus = status;
}

MouseInput::MouseStatus MouseInput::GetMouseStatus() {
    return mouseStatus;
}

void MouseInput::SetMousePosition(double x, double y) {
    mouseXPosition = x;
    mouseYPosition = y;
}
