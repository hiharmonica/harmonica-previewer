#include <string>
#include <chrono>
#include <iostream>
#include "common.h"
#include "screen_device_proxy.h"
#include "VirtualScreen.h"


VirtualScreen::VirtualScreen() {}

VirtualScreen::~VirtualScreen() {}

void VirtualScreen::Init(uint16_t width, uint16_t height) {
    screenBuffer = new uint8_t[width * height * 4];
    memset(screenBuffer, 0, width * height * 4);
    OHOS::ScreenDeviceProxy::GetInstance()->SetScreenSize(width, height);
}

void VirtualScreen::Flush(int16_t x1, int16_t y1, int16_t x2, int16_t y2, const OHOS::ScreenBufferType *colorBuffer) {
    for (int i = y1; i <= y2; ++i) {
        for (int j = x1; j <= x2; ++j) {
            uint8_t *curPixel = screenBuffer + (i * OHOS::ScreenDeviceProxy::GetInstance()->GetScreenWidth() + j) * 4;
            *(curPixel++) = (*colorBuffer).alpha;
            *(curPixel++) = (*colorBuffer).red;
            *(curPixel++) = (*colorBuffer).green;
            *(curPixel++) = (*colorBuffer).blue;
            colorBuffer++;
        }
    }
    isChanged = true;
    OHOS::ScreenDeviceProxy::GetInstance()->OnFlushReady();
}

uint16_t VirtualScreen::GetWidth() const {
    return OHOS::ScreenDeviceProxy::GetInstance()->GetScreenWidth();
}

uint16_t VirtualScreen::GetHeight() const {
    return OHOS::ScreenDeviceProxy::GetInstance()->GetScreenHeight();
}

