#include "AsyncWorkManager.h"

AsyncWorkManager::AsyncWorkManager() {

}

AsyncWorkManager::~AsyncWorkManager() {

}

void AsyncWorkManager::ExecAllAsyncWork() {
    mutex.lock();
    auto tempList = workList;
    workList.clear();
    mutex.unlock();

    for (auto work : tempList) {
        work.first(work.second);
    }
}

void AsyncWorkManager::ClearAllAsyncWork() {
    mutex.lock();
    workList.clear();
    mutex.unlock();
}

void AsyncWorkManager::AppendAsyncWork(OHOS::ACELite::AsyncWorkHandler work, void *arg) {
    mutex.lock();
    workList.push_back(std::make_pair(work, arg));
    mutex.unlock();
}
