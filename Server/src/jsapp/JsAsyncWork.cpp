#include "js_async_work.h"
#include "iostream"
#include "ace_log.h"

bool OHOS::ACELite::JsAsyncWork::DispatchAsyncWork(OHOS::ACELite::AsyncWorkHandler workHandler, void *data) {
    if (workHandler != nullptr) {
        workHandler(data);
        return true;
    }
    std::cout << "[Error]: workHandler is nullptr" << std::endl;
    return true;
}
