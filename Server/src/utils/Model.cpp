#include "parameter.h"
#include "securec.h"
#include <string>
#include "ability_env.h"
#include "common.h"

static char OHOS_BRAND[] = {"深鸿会"};
static char OHOS_PRODUCT_MODEL[] = {"SmartVision"};
static char OHOS_MANUFACTURE[] = {"深鸿会"};
const std::string dataPath = ApplicationPath() + "/file_system";
const char *path = dataPath.c_str();

char* GetBrand() {
    char* value = (char*)malloc(strlen(OHOS_BRAND) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_BRAND) + 1, OHOS_BRAND) != 0) {
        free(value);
        return NULL;
    }
    return value;
}
char* GetManufacture() {
    char* value = (char*)malloc(strlen(OHOS_MANUFACTURE) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_MANUFACTURE) + 1, OHOS_MANUFACTURE) != 0) {
        free(value);
        return NULL;
    }
    return value;
}
char* GetProductModel() {
    char* value = (char*)malloc(strlen(OHOS_PRODUCT_MODEL) + 1);
    if (value == NULL) {
        return NULL;
    }
    if (strcpy_s(value, strlen(OHOS_PRODUCT_MODEL) + 1, OHOS_PRODUCT_MODEL) != 0) {
        free(value);
        return NULL;
    }
    return value;
}
#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif
const char *GetBundleName() {
    std::string bundleName;
    if(!cliParam.projectName.empty()) {
        bundleName = cliParam.projectName;
    }else {
        bundleName = "com.shenhonghui.previewer";
    }
    return bundleName.c_str();
}

const char *GetSrcPath() {
    return path;
}

const char *GetDataPath() {
    return path;
}
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif
