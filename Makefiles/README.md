# 构建鸿蒙ACE静态库

### 搭建开发环境
- 根据说明文档下载鸿蒙全部代码，一般采取第四种方式获取最新代码(请保证代码为最新) [源码获取](https://gitee.com/openharmony/docs/blob/master/get-code/%E6%BA%90%E7%A0%81%E8%8E%B7%E5%8F%96.md)
- Windows下载编译环境 MinGW GCC 7.3.0版本 请添加环境变量
- IDE 可以使用两种 CLion和Qt,CLion不带有环境需要安装MinGW才可以开发,Qt自带MinGW环境，推荐安装[Qt 5.14.2](http://download.qt.io/archive/qt/5.14/5.14.2/)
- 使用CMake来管理项目 请保证CMake版本大于3.16.5

### 修改工程
- 请使用本项目Harmony目录下所有的CMakeLists.txt文件替换鸿蒙中的相同目录下的CMakeLists.txt 文件
- 请注释掉Harmony/foundation/ace/frameworks/lite/targets/simulator/win/acelite_config.h 中的部分定义，如果启用其中的定义，需要自己实现相应的API函数，否则在构建预览器项目中会导致报错。可以使用提供的acelite_config.h替换，也可以自行修改。
- 编译主要涉及以下几个目录
  - base/global
  - foundation/aafwk
  - foundation/ace
  - foundation/graphic
  - third_party/bounds_checking_function
  - third_party/cJSON
  - third_party/freetype
  - third_party/jerryscript
  - third_party/libjpeg
  - third_party/libpng
  - third_party/zlib
  - utils/native

### 编译
- 在Harmony目录下创建cmake-build目录
```
cd Harmony
mkdir cmake-build
cd cmake-build
```
- 使用cmake创建makefile
```
cmake .. -G "MinGW Makefiles"
-- The C compiler identification is GNU 7.3.0
-- The CXX compiler identification is GNU 7.3.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: C:/Qt/Qt5.14.2/Tools/mingw730_64/bin/gcc.exe -
skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: C:/Qt/Qt5.14.2/Tools/mingw730_64/bin/g++.exe
- skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
C:/Users/hpf19/Desktop/111222/Harmony/third_party/libpng
-- Looking for stddef.h
-- Looking for stddef.h - found
-- Looking for stdlib.h
-- Looking for stdlib.h - found
-- Looking for string.h
-- Looking for string.h - found
-- Looking for size_t
-- Looking for size_t - not found
-- Looking for setmode
-- Looking for setmode - found
-- Performing Test CHAR_IS_UNSIGNED
-- Performing Test CHAR_IS_UNSIGNED - Failed
-- Performing Test RIGHT_SHIFT_IS_UNSIGNED
-- Performing Test RIGHT_SHIFT_IS_UNSIGNED - Failed
-- Configuring done
-- Generating done
-- Build files have been written to: C:/Users/hpf19/Desktop/111222/Harmony/cmake
-build
```
- 构建静态库
```
mingw32-make.exe
```
- 生成结果 

|静态文件|文件夹|
|:-:|:-:|
|libglobal_resmgr.a|base|
|libjerry-core.a|foundation/ace|
|libjerry-ext.a|foundation/ace|
|libjerry-libm.a|foundation/ace|
|libjerry-port-default.a|foundation/ace|
|libjerry-port-default-minimal.a|foundation/ace|
|libjsfwk.a|foundation/ace|
|libui.a|foundation/graphic|
|libfreetype.a|foundation/graphic|
|liblibjpeg.a|foundation/graphic|
|liblibpng.a|foundation/graphic|
|libzlib.a|foundation/graphic|
|libutils_jsapi.a|utils|

### 问题
- 和ACE一起编译生成的jerry静态库会比单独编译的静态库大100KB,会导致JsApp关闭时出现严重BUG导致程序崩溃。建议大家单独编译Jerry库，在third_party/jerryscript下面建立一个cmake-build目录和上面的一样的操作方法就可以生成Jerry静态库了。
